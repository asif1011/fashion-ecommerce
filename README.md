# Fashion Evolution #

This Project **Fashion Evolution** is to enhance the fashion designing skills for beginners professionals also.

### Steps to setup project locally ###

* Ask Administrator to give you SSH keys, You can provide your public SSH key to the admin to add it in the account and give you access.
* Clone the project repository using SSH access and command will be

```
#!python

```

* Now create a virtual environment for the fashion.
* Activate the virtual Environment and make the project repository as your current directory.
* Install all other dependencies by running.

```
#!python

pip install -r requirements.txt
```
* Create a local settings file using the local_settings_sample and you should be able to run server using

```
#!python

python manage.py runserver

```


### Contact Support? ###

* If you are still facing any problem, Please contact at aakhtar189@gmail.com