from __future__ import unicode_literals

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import password_validation
from django.contrib.auth.forms import PasswordResetForm
from accounts.models import PasswordResetAuth


class LoginForm(forms.Form):
    email = forms.EmailField(max_length=75, widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        email = cleaned_data.get("email", "")

        # user = User.objects.get(username=username)
        # if not user.password == cleaned_data["password"]:
        #     raise forms.ValidationError("Password is w")

        if not User.objects.filter(email=email).exists():
            raise forms.ValidationError("Account doesn't exist with this email id. Please try again!")
        else:
            user = User.objects.get(email=email)
            cleaned_data["username"] = user.username

        return cleaned_data


class PasswordChangeForm(forms.Form):
    old_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(PasswordChangeForm, self).__init__(*args, **kwargs)

    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        old_password = self.cleaned_data["old_password"]

        if not self.user.check_password(old_password):
            raise forms.ValidationError("Your old password was entered incorrectly. Please enter it again.")

        return old_password

    def clean_confirm_password(self):
        new_password = self.cleaned_data.get('new_password')
        confirm_password = self.cleaned_data.get('confirm_password')

        if new_password and confirm_password:
            if new_password != confirm_password:
                raise forms.ValidationError("The two password fields didn't match.")

        password_validation.validate_password(confirm_password, self.user)

        return confirm_password

    def save(self, commit=True):
        password = self.cleaned_data["new_password"]
        self.user.set_password(password)

        if commit:
            self.user.save()

        return self.user


class ForgetPasswordForm(forms.Form):
    email = forms.EmailField(max_length=75, widget=forms.TextInput(attrs={'class': 'form-control'}))

    def clean_email(self):
        cleaned_data = super(ForgetPasswordForm, self).clean()
        email = cleaned_data.get("email", "")
        if not User.objects.filter(email=email).exists():
            raise forms.ValidationError("Account doesn't exist with this email id. Please try again!")
        return email


class ResetPasswordForm(forms.Form):
    new_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def __init__(self, *args, **kwargs):
        self.token = kwargs.pop('token', None)
        super(ResetPasswordForm, self).__init__(*args, **kwargs)

    def clean_confirm_password(self):
        cleaned_data = super(ResetPasswordForm, self).clean()
        new_password = cleaned_data.get("new_password", "")
        confirm_password = cleaned_data.get("confirm_password", "")

        if new_password != confirm_password:
            raise forms.ValidationError("Password did not match. Make it correct.")

        if not PasswordResetAuth.objects.filter(token=self.token, is_expired=False).exists():
            raise forms.ValidationError("Either you are not an identified user or token has been expired. So please click on forget password.")

        return confirm_password


class ContactForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}))
    email = forms.EmailField(max_length=75, widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}))
    phone_no = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'required': 'required'}))
    message = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'cols': '0', 'rows': '8', 'required': 'required'}))


class NewsletterForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(attrs={'type': 'text', 'class': 'form-control', 'placeholder': 'Email'}))
