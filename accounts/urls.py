from django.conf.urls import include, url

urlpatterns = [
    url(r'login/$', 'accounts.views.user_login', name='user_login'),
    url(r'^forget-password/$', 'accounts.views.forget_password', name='forget_password'),
    url(r'^reset-password/(?P<token>[a-zA-Z0-9]*)/$', 'accounts.views.user_reset_password', name="user_reset_password"),
    url(r'^change-password/$', 'accounts.views.change_password', name='change_password'),
    url(r'^newsletter/$', 'accounts.views.newsletter', name="newsletter")
]    

