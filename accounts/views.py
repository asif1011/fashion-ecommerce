import json

from django.conf import settings
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.crypto import get_random_string
from django.contrib.auth import update_session_auth_hash
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse
from django.contrib import messages

from fashion.utils import SendEmail
from accounts.models import PasswordResetAuth, Newsletter
from accounts.forms import LoginForm, PasswordChangeForm, ForgetPasswordForm, ResetPasswordForm, ContactForm, NewsletterForm

from designer.models import Designer

def about_us(request):
    return render_to_response("about_us.html", {'current': 'about_us'}, context_instance=RequestContext(request))


def contact_us(request):
    contact_form = ContactForm(request.POST or None)

    if request.method == 'POST':
        if contact_form.is_valid():
            msg = SendEmail(request)
            msg.send(recipient=[settings.DEFAULT_CONTACT_EMAIL], template_path='email_messages/contact_us.html',
                context={
                    'first_name': contact_form.cleaned_data["first_name"],
                    'last_name': contact_form.cleaned_data["last_name"],
                    'phone_no': contact_form.cleaned_data["phone_no"],
                    'email': contact_form.cleaned_data["email"],
                    'message': contact_form.cleaned_data["message"]
                },
                subject='[Fashion Celio] {} sent a query!'.format(contact_form.cleaned_data["first_name"])
            )

            msg.send(recipient=[contact_form.cleaned_data["email"]], template_path='email_messages/contact_reply.html',
                    context={
                        'first_name': contact_form.cleaned_data["first_name"],
                    },
                    subject='Thank You for Contacting Us!'
                )
            messages.add_message(request, messages.INFO, "Thankyou for contacting us! We will get back to you shortly!".format(contact_form.cleaned_data["first_name"]))
            return redirect("/")

    return render_to_response("contact_us.html", {'current': 'contact_us', 'form': contact_form}, context_instance=RequestContext(request))


def newsletter(request):
    if request.method == 'POST' and request.is_ajax() and request.POST.get('email', ''):
        newsletter, _ = Newsletter.objects.get_or_create(email=request.POST.get('email', ''))

    return HttpResponse(json.dumps({'status': True}))


def user_login(request):

    if request.user.is_authenticated():
        return redirect("/")

    form = LoginForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)

            if user is not None:
                if user.is_active:
                    login(request, user)
                    if Designer.objects.filter(user=user).exists():
                        return redirect(request.GET.get("next", "/designer/%s/" % user.username))
                    else:
                        return redirect(request.GET.get("next", "/"))
                else:
                    error_message = "* This account is not activated yet, Please contact support for account activation!"
                    return render_to_response("accounts/login.html", {
                        "form": form,
                        "current": "login",
                        "error_message": error_message
                    }, context_instance=RequestContext(request))

            error_message = "* Email Id and password did not match. Please try again!"
            return render_to_response("accounts/login.html", {
                "form": form,
                "current": "login",
                "error_message": error_message
            }, context_instance=RequestContext(request))

        return render_to_response("accounts/login.html", {
            "form": form,
            "current": "login"
        }, context_instance=RequestContext(request))

    return render_to_response("accounts/login.html", {
        "form": form,
        "current": "login"
    }, context_instance=RequestContext(request))


@login_required
def change_password(request):
    form = PasswordChangeForm(request.user, request.POST or None)
    user = User.objects.get(id=request.user.id)

    if request.method == "POST":
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, user)
            messages.add_message(request, messages.INFO, "Hi {}, Your password has been changed successfully. Please login!".format(request.user.first_name))
            return redirect('/')
                
    return render_to_response("accounts/change_password.html", {
        "current": "reset_password",
        "form": form
    }, context_instance=RequestContext(request))


def forget_password(request):
    """
        Function is used for the forgot password functionality.
        In this password is resetted by sending a mail to registered users.
        Function also maintains a token for resetting the password which is later used to
        identify the user.
    """

    if request.user.is_authenticated():
        return redirect("/")

    form = ForgetPasswordForm(request.POST or None)

    if form.is_valid():
        email = form.cleaned_data["email"]
        token = get_random_string(length=11)
        PasswordResetAuth.objects.create(email=email, token=token)

        msg = SendEmail(request)
        msg.send(recipient=[email], template_path='email_messages/password_reset.html',
            context={
                'reset_link': settings.SITE_URL+"/accounts/reset-password/{}".format(token)
            },
            subject='Reset Password!'
        )

        return redirect(reverse('password_reset_done'))

    return render_to_response("accounts/user_identify.html", {
        "current": "forgot_password",
        "form": form
    }, context_instance=RequestContext(request))


def user_reset_password(request, token):
    """
        Function is used to reset the password of the user using the token identification
        which was created in forgot password.
    """

    if request.user.is_authenticated():
        return redirect("/")

    form = ResetPasswordForm(request.POST or None, token=token)

    try:
        password_reset_auth = PasswordResetAuth.objects.get(token=token, is_expired=False)
    except PasswordResetAuth.DoesNotExist:
        raise Http404

    if request.method == "POST":
        if form.is_valid():
            user = User.objects.get(email=password_reset_auth.email)
            new_password = form.cleaned_data["new_password"]
            user.set_password(new_password)
            user.save()

            password_reset_auth.is_expired = True
            password_reset_auth.save()
            return redirect(reverse('user_login'))

    return render_to_response("accounts/user_reset_password.html", {
        "current": "reset_password",
        "form": form
    }, context_instance=RequestContext(request))


def newsletter(request):
    """
    This function is used to subscribe the newsletter for the input email address.
    """
    response = {'status': False, 'errors': []}

    newsletterform = NewsletterForm(request.POST or None)

    if request.method == 'POST':
        if newsletterform.is_valid():
            Newsletter.objects.get_or_create(email=newsletterform.cleaned_data['email'])
            response['status'] = True

        else:
            for key, value in newsletterform.errors.iteritems():
                tmp = {'key': key, 'error': value.as_text()}
                response['errors'].append(tmp)

    return HttpResponse(json.dumps(response))


def blog(request):
    return render_to_response("blog.html", {
        
    }, context_instance=RequestContext(request)) 