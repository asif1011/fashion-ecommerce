from __future__ import unicode_literals
from datetime import datetime

from django import forms
from django.conf import settings
from django.forms import FileInput
from django_countries import countries
from django.contrib.auth.models import User
from django.contrib.auth import password_validation
from buyer.models import Buyer, GENDER_CHOICES, Feedback
from designer.models import Designer, Size


class SignUpBuyerForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean_email(self):
        try:
            existing_user = User.objects.get(email__iexact=self.cleaned_data['email'])
            if existing_user:
                self._errors["email"] = self.error_class(["An account already exists under this email address. Please use the forgot password function to log in."])
        except User.MultipleObjectsReturned:
            self._errors["email"] = self.error_class(["An account already exists under this email address. Please use the forgot password function to log in."])
        except:
            pass

        return self.cleaned_data['email']


class EditBuyerForm(forms.ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'email', 'readonly': 'readonly'}))

    class Meta:
        model=Buyer
        exclude = ['user', 'favorite_design']
        widgets = {
            'avatar': FileInput(),
        }

    def __init__(self, *args, **kwargs):
        super(EditBuyerForm, self).__init__(*args, **kwargs)
        self.fields['phone_no'].widget.attrs.update({'class': 'form-control'})
        self.fields['address'].widget.attrs.update({'class': 'form-control', 'cols': '', 'rows': '3'})
        self.fields['city'].widget.attrs.update({'class': 'form-control'})
        self.fields['gender'].widget.attrs.update({'class': 'form-control'})
        self.fields['state'].widget.attrs.update({'class': 'form-control'})
        self.fields['postal_code'].widget.attrs.update({'class': 'form-control'})
        self.fields['country'].widget.attrs.update({'class': 'form-control'})


class FeedbackForm(forms.Form):
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    rating = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    reviews = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'rows': '4'}))


class DesignRequestForm(forms.Form):
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control  myinputclass', 'placeholder': 'Give Title of the Design', 'maxlength': '100'}))
    size = forms.CharField(widget=forms.Select(attrs={'class': 'form-control multiselect mymultiselect', 'required': 'required'}))
    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control myinputclass', 'rows': 5, 'placeholder': 'Describe Your Design', 'maxlength': '1000'}))
    design_image = forms.ImageField(required=True)
    designer = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control myinputclass'}))
    price = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Price'}))

    def clean_price(self):
        price = self.cleaned_data.get('price')

        try:
            price = float(price)
        except:
            raise forms.ValidationError('Enter a number.')

        return price


    def __init__(self, *args, **kwargs):
        super(DesignRequestForm, self).__init__(*args, **kwargs)
        self.fields['designer'] = forms.ChoiceField(
            choices=[(str(designer.id), str(designer.user.get_full_name())) for designer in Designer.objects.all()]
        )
        self.fields['size'] = forms.MultipleChoiceField(
            choices=[(size.id, str(size.name)) for size in Size.objects.all()]
        )
        self.fields['size'].required = True
        self.fields['designer'].widget.attrs.update({'class': 'form-control'})
        self.fields['designer'].required = True