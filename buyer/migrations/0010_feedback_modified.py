# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-03-17 20:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('buyer', '0009_feedback_created'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='modified',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
