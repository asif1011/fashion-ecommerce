# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-05-14 05:45
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('buyer', '0020_designrequest_size'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='designrequest',
            name='size',
        ),
    ]
