from __future__ import unicode_literals

import os
from datetime import datetime

from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django_countries.fields import CountryField
from django.core.files.storage import FileSystemStorage
from autoslug import AutoSlugField
from designer.models import Design, Size, Designer

media_storage = FileSystemStorage(location=settings.BASE_DIR, base_url="/media/")


GENDER_CHOICES = (
    ('Male', 'Male'),
    ('Female', 'Female'),
    ('Others', 'Others'),
)

ORDER_ITEM_STATUS = (
    ('Requested', 'Requested'),
    ('OutForDelivery', 'OutForDelivery'),
    ('Delivered', 'Delivered')
)

ORDER_STATUS = (
    ('Draft', 'Draft'),
    ('Success', 'Success'),
    ('Canceled', 'Canceled'),
    ('Failed', 'Failed')            
)

PAYMENT_CHOICE = (
    ('COD', 'COD'),
    ('ONLINE', 'ONLINE')
)

DESIGN_REQUEST_STATUS = (
    ('Pending', 'Pending'),
    ('Accepted', 'Accepted'),
    ('Rejected', 'Rejected')
)

class Buyer(models.Model):
    user = models.ForeignKey(User)
    avatar = models.ImageField(upload_to="profile/buyer/", null=True, blank=True)
    phone_no = models.CharField(max_length=12, blank=True, null=True)
    gender = models.CharField(max_length=60, null=True, blank=True, choices=GENDER_CHOICES)
    address = models.TextField(null=True, blank=True)
    city  = models.CharField(max_length=30, blank=True, null=True)
    state = models.CharField(max_length=30, blank=True, null=True)
    postal_code = models.CharField(max_length=12, blank=True, null=True)
    country = CountryField(blank_label="(select country)", null=True, blank=True)
    favorite_design = models.ManyToManyField(Design)

    def __str__(self):
        return u'%s %s' %(self.user.first_name, self.user.last_name)


class RecentlyViewed(models.Model):
    design = models.ForeignKey(Design)
    user = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


class CartItems(models.Model):
    design = models.ForeignKey(Design)
    size = models.ForeignKey(Size)
    buyer = models.ForeignKey(Buyer)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


class Feedback(models.Model):
    buyer = models.ForeignKey(Buyer)
    design = models.ForeignKey(Design)
    title = models.CharField(max_length=50, blank=True, null=True)
    rating = models.FloatField(default=0.0)
    reviews = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    modified = models.DateTimeField(auto_now=True, null=True, blank=True)
    is_public = models.BooleanField(default=True)


class Order(models.Model):
    buyer = models.ForeignKey(Buyer, null=True, blank=True)
    first_name = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=100, null=True, blank=True)
    phone_no = models.CharField(max_length=10, null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    state = models.CharField(max_length=100, null=True, blank=True)
    postal_code = models.CharField(max_length=12, blank=True, null=True)
    country = models.CharField(max_length=255, null=True, blank=True)
    order_time = models.DateTimeField(auto_now_add=True)
    total_price = models.DecimalField(max_digits=18, decimal_places=2)
    order_status = models.CharField(max_length=30, choices=ORDER_STATUS, null=True, blank=True)
    payement_method = models.CharField(max_length=30, choices=PAYMENT_CHOICE, null=True, blank=True)
    charge_id = models.CharField(max_length=100, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __unicode__(self):
        return u'Order by %s' % self.buyer


class OrderItem(models.Model):  
    order = models.ForeignKey(Order)
    buyer = models.ForeignKey(Buyer, null=True, blank=True)
    design = models.ForeignKey(Design, on_delete=models.PROTECT)
    size = models.ForeignKey(Size, null=True, blank=True)
    unit_price = models.DecimalField(max_digits=18, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True, null=True, blank=True) 
    order_item_status = models.CharField(max_length=30, choices=ORDER_ITEM_STATUS, default='Requested')
    delivery_day = models.CharField(max_length=50, null=True, blank=True)

    def __unicode__(self):
        return u'%s- %s' % (self.design, self.order)


class DesignRequest(models.Model):
    title = models.CharField(max_length=255)
    size = models.ManyToManyField(Size)
    description = models.TextField(null=True, blank=True)
    design_image = models.ImageField(upload_to="request_design/designs/", null=True, blank=True)
    buyer = models.ForeignKey(Buyer)
    designer = models.ForeignKey(Designer)
    request_status = models.CharField(max_length=30, choices=DESIGN_REQUEST_STATUS, default='Pending')
    reject_reason = models.TextField(null=True, blank=True)
    price = models.DecimalField(max_digits=18, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)