from django.conf.urls import url


urlpatterns = [
    url(r'^register/$', 'buyer.views.register', name='register'),
    url(r'^(?P<username>\w+)/$', 'buyer.views.buyer_profile', name='buyer_profile'),
    url(r'^edit-profile/$', 'buyer.views.edit_buyer', name='edit_buyer'),
    url(r'^recent-design/$', 'buyer.views.my_recent_views', name='my_recent_views'),
    url(r'^favorite-design/$', 'buyer.views.my_favorites', name='my_favorites'),
   	url(r'^add-or-remove-favorite/$', 'buyer.views.add_or_remove_favorite',  name='add_or_remove_favorite'),
   	url(r'^add-to-cart/$', 'buyer.views.add_to_cart',  name='add_to_cart'),
    url(r'^delete-recently-viewed/$', 'buyer.views.delete_recent_views', name='delete_recent_views'),
    url(r'^create-feedback/$', 'buyer.views.create_feedback', name='create_feedback'),
    url(r'^my-cart/$', 'buyer.views.my_carts', name='my_carts'),
    url(r'^delete-cart-items/$', 'buyer.views.delete_cart_items', name='delete_cart_items'),
    url(r'^deliver-to/$', 'buyer.views.get_delivery_address', name='get_delivery_address'),
    url(r'^payement-method/$', 'buyer.views.get_payment_method', name='get_payment_method'),
    url(r'^my-orders/$', 'buyer.views.orders_list', name='orders_list'),
    url(r'^order-item/(?P<order_item_id>[0-9]+)/$', 'buyer.views.order_detail', name='order_detail'),
    url(r'^design-request/$', 'buyer.views.design_request', name='design_request'),
    url(r'^my-design-request/$', 'buyer.views.requested_design', name='requested_design'),

]
