import json, os
import random

from datetime import date, datetime, time, timedelta

from string import ascii_uppercase
from PIL import Image

from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib.auth import authenticate, login, get_backends
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.crypto import get_random_string
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q, Count
from django.http import HttpResponse, Http404
from django_countries import countries
from django.template.loader import render_to_string
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.utils.crypto import get_random_string
from django.utils import timezone
from django.core.files.base import ContentFile
from fashion.utils import SendEmail
from designer.utils import create_username
from buyer.models import Buyer, RecentlyViewed, Feedback, CartItems, Order, OrderItem, DesignRequest
from buyer.forms import SignUpBuyerForm, EditBuyerForm, FeedbackForm, DesignRequestForm
from designer.models import Design, Size, FinancialActivities, Designer

def register(request):
   
    if request.method == "POST":
        form = SignUpBuyerForm(request.POST, request.FILES)

        if form.is_valid():
            user = User.objects.create(
                username=create_username('%s %s' % (form.cleaned_data["first_name"], form.cleaned_data["last_name"])),
                first_name=form.cleaned_data["first_name"],
                last_name=form.cleaned_data["last_name"],
                email=form.cleaned_data["email"],
                is_active=True      
            )
       
            buyer = Buyer.objects.create(
                user=user,
            )
            password = form.cleaned_data["password"]

            if password:
                user.set_password(password)
                user.save()

            msg = SendEmail()
            msg.send(recipient=[buyer.user.email], template_path='email_messages/new_buyer_email_buyer.html',
                context={
                    'buyer': buyer,
                    'password' : password
                },
                subject='Welcome to Fashion Evolution!'
            )

            if request.FILES:
                avatar = request.FILES.get("avatar")
                buyer.avatar = avatar

            buyer.save()

            if buyer and not request.user.is_authenticated():
                backend = get_backends()[0]
                buyer.user.backend = "%s.%s" % (backend.__module__, backend.__class__.__name__)
                login(request, buyer.user)
                messages.add_message(request, messages.INFO, "Thanks you for Registration. Get the latest design by the world class designer")
                return redirect("/")

        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}
                print tmp
    else:
        form = SignUpBuyerForm()

    return render_to_response("buyer/registration.html", {
        "form": form,
    }, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def buyer_profile(request, username):
    if request.user.is_superuser:
        return render_to_response("admin.html", {  
        }, context_instance=RequestContext(request))
    
    buyer = Buyer.objects.get(user__username=username)

    if not request.user == buyer.user:
        return(redirect("/"))

    else:    
        return render_to_response("buyer/profile.html", {
            "current" : "profile",
            "buyer" : buyer,
            
        }, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def edit_buyer(request):

    buyer = Buyer.objects.get(user=request.user)

    if request.method == "POST":
        form = EditBuyerForm(request.POST, request.FILES, instance=buyer)

        if form.is_valid():
            obj = form.save(commit=False)
            obj.user.first_name = form.cleaned_data['first_name']
            obj.user.last_name = form.cleaned_data['last_name']

            obj.user.save()
            obj.save()

            messages.add_message(request, messages.INFO, "Hi {}, Your profile has been updated successfully.".format(buyer.user.first_name))

            return redirect(reverse('buyer_profile', kwargs={'username': buyer.user.username}))

        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}
                print tmp

        return render_to_response("buyer/edit_profile.html", {
            "form": form,
            "buyer": buyer,
        }, context_instance=RequestContext(request))

    else:
        form = EditBuyerForm(instance=buyer, initial={
            'first_name': buyer.user.first_name,
            'last_name': buyer.user.last_name,
            'email': buyer.user.email,
        })

        return render_to_response("buyer/edit_profile.html", {
            "current": "edit",
            "form": form,
            "buyer": buyer,
        }, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def my_recent_views(request):

    recently_viewed = RecentlyViewed.objects.filter(user=request.user).order_by('-modified')[:16]

    return render_to_response("buyer/recently_viewed.html", {
        "current" : "recently",
        "recently_viewed" : recently_viewed,
        
    }, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def my_favorites(request):

    buyer = Buyer.objects.get(user=request.user)

    return render_to_response("buyer/favorite.html", {
        "current" : "favorite",
        "buyer" : buyer,
        
    }, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def add_or_remove_favorite(request):
    response = {"status": False, "errors": []}

    if request.is_ajax() and request.POST['action'] == 'create_favorite':
        design_id = request.POST['id']
        design = Design.objects.get(id=design_id)
        buyer = Buyer.objects.get(user=request.user)
        buyer.favorite_design.add(design)
        buyer.save()
        response["text"] = "{} added to favorite".format(design)

    if request.is_ajax() and request.POST['action'] == 'remove_favorite':
        design_id = request.POST['id']
        design = Design.objects.get(id=design_id)
        buyer = Buyer.objects.get(user=request.user)
        buyer.favorite_design.remove(design)
        buyer.save()
        response["text"] = "{} removed from favorite".format(design)

    response["status"] = True
    response["id"] = design_id
    return HttpResponse(json.dumps(response))


@login_required
def delete_recent_views(request):
    my_recent_views = RecentlyViewed.objects.filter(user=request.user)
    my_recent_views.delete()
    return redirect(reverse('my_recent_views'))


@login_required
def create_feedback(request):

    if request.is_ajax():

        response = {"status": False, "errors": []}

        design_id = request.POST["design"]

        design = Design.objects.get(id=design_id)

        buyer = Buyer.objects.get(user=request.user)

        if request.method == "POST":
            form= FeedbackForm(request.POST)

            if form.is_valid():
                feedback = Feedback.objects.create(
                    buyer = buyer,
                    design = design,
                    rating=form.cleaned_data["rating"],
                    title=form.cleaned_data["title"],
                    reviews=form.cleaned_data["reviews"],
                )

                response["status"] = True

            else:
                for key, value in form.errors.items():
                        tmp = {'key': key, 'error': value.as_text()}
                        response['errors'].append(tmp)
                        
        return HttpResponse(json.dumps(response))


@login_required
def add_to_cart(request):
    if not request.user.is_authenticated():
        return redirect('/accounts/login')

    response = {"status": False, "errors": []}

    if request.is_ajax() and request.POST['action'] == 'None':
        design_id = request.POST['design_id']
        size_id = request.POST['size_id']

        design = Design.objects.get(id=design_id)
        size = Size.objects.get(id=size_id)
        buyer = Buyer.objects.get(user=request.user)

        if CartItems.objects.filter(design=design, buyer=buyer, size=size).exists():
            response["text"] = "{} is already in the cart".format(design)
        else:
            response["text"] = "{} added to the cart".format(design)

        cartitems, created = CartItems.objects.get_or_create(
            buyer = buyer,
            design = design,
            size = size,
        )
        if not created:
            cartitems.save()  
    
    if request.is_ajax() and request.POST['action'] == 'add_and_buy':
        design_id = request.POST['design_id']
        size_id = request.POST['size_id']

        design = Design.objects.get(id=design_id)
        size = Size.objects.get(id=size_id)
        buyer = Buyer.objects.get(user=request.user)

        cartitems, created = CartItems.objects.get_or_create(
            buyer = buyer,
            design = design,
            size = size,
        )

        if not created:
            cartitems.save()

        response["next"] = "/buyer/my-cart/"

    response["status"] = True   
    return HttpResponse(json.dumps(response))


@login_required
def my_carts(request):
    buyer = Buyer.objects.get(user=request.user)
    my_carts = CartItems.objects.filter(buyer=buyer).order_by('-created')
    total_items = my_carts.count()

    price_list = my_carts.values_list('design__price', flat=True)
    total_sum  = sum(price_list)

    return render_to_response("buyer/mycart.html", {
        "total_sum": total_sum,
        "total_items": total_items,
        "my_carts": my_carts,
    }, context_instance=RequestContext(request))


@login_required
def delete_cart_items(request):
    response = {"status": False, "errors": []}
    items_id = request.GET.get('items_id', '')
    buyer = Buyer.objects.get(user=request.user)

    cartitem = CartItems.objects.get(id=items_id)
    if cartitem.buyer == buyer:
        cartitem.delete()

    response["status"] = True
    left_items = CartItems.objects.all().count()
    if left_items == 0:
        response["left_items"] = True
    return HttpResponse(json.dumps(response))


@login_required(login_url=settings.LOGIN_URL)
def get_delivery_address(request):

    buyer = Buyer.objects.get(user=request.user)
    my_carts = CartItems.objects.filter(buyer=buyer).order_by('-created')
    if my_carts.count() == 0:
        return redirect(reverse("my_carts"))

    price_list = my_carts.values_list('design__price', flat=True)
    total_sum  = sum(price_list)
    total_sum = format(total_sum, '.2f')

    if request.method == "POST":

        first_name = request.POST.get("first_name", '')
        last_name = request.POST.get("last_name", '')
        phone_no = request.POST.get("phone_no", '')
        city = request.POST.get("city", '')
        address = request.POST.get("address", '')
        state = request.POST.get("state", '')
        postal_code = request.POST.get("postal_code", '')
        country = request.POST.get("country", '')

        order, created = Order.objects.get_or_create(
            buyer=buyer,
            first_name=first_name,
            last_name=last_name,
            phone_no=phone_no,
            city=city,
            address=address,
            state=state,
            postal_code=postal_code,
            country=country,
            total_price=total_sum,
            order_status="Draft"
        )

        request.session['order'] = order.id

        return redirect(reverse("get_payment_method"))
        
    return render_to_response("buyer/get_address.html", {
        "countries": countries,                    
        "buyer": buyer,
        "total_sum": total_sum,
    }, context_instance=RequestContext(request))


@login_required
def get_payment_method(request):
    if not 'order' in request.session:
        return redirect(reverse('get_delivery_address'))
    else:
        order = Order.objects.get(id=request.session['order'])

    buyer = Buyer.objects.get(user=request.user)
    my_carts = CartItems.objects.filter(buyer=buyer)

    price_list = my_carts.values_list('design__price', flat=True)
    total_sum  = sum(price_list)
    total_sum = format(total_sum, '.2f')

    if request.method == "POST":

        pay_option = request.POST.get("pay_option", '')

        if pay_option == "cod":
            charge_id = get_random_string(length=27)
            order.charge_id = charge_id
            order.payement_method = "COD"
            order.order_status = "Success"
            order.save()

            for items in my_carts:
                order_item = OrderItem.objects.create(
                    buyer=items.buyer,
                    order=order,
                    design=items.design,
                    size=items.size,
                    unit_price=items.design.price,
                    order_item_status="Requested",
                )
            order_item = OrderItem.objects.filter(order=order)

            msg = SendEmail()
            msg.send(recipient=[buyer.user.email], template_path='email_messages/order_notification_to_buyer.html',
                context={
                    'order': order,
                    'order_item': order_item

                },
                subject='Thanks for Your Order!'
            )
            for design in order_item:
                msg.send(recipient=[design.design.designer.user.email], template_path='email_messages/order_notification_to_desginer.html',
                    context={
                        'design': design
                    },
                    subject='Your designs has been sell out!'
                )

                financial_activities = FinancialActivities.objects.create(
                    designer=design.design.designer,
                    design=design.design,
                    status="Pending",
                    price=design.unit_price,
                )
            del request.session["order"]

            for items in my_carts:
                items.delete()

            messages.add_message(request, messages.INFO, "Your Oder hasbeen placed successfully.")

            return redirect(reverse("orders_list"))

    return render_to_response("buyer/get_payment_method.html", {
        "total_sum": total_sum
    }, context_instance=RequestContext(request))


@login_required
def orders_list(request):
    buyer = Buyer.objects.get(user=request.user)
    order_items = OrderItem.objects.filter(buyer=buyer).order_by('-created')

    return render_to_response("buyer/orders.html", {
        "order_items": order_items
    }, context_instance=RequestContext(request))


@login_required
def order_detail(request, order_item_id):

    order_item = OrderItem.objects.get(id=order_item_id)
    if not request.user == order_item.order.buyer.user:
        return(redirect("/"))

    return render_to_response("buyer/order_detail.html", {
        "order_item": order_item
    }, context_instance=RequestContext(request))


@login_required
def design_request(request):
    try:
        buyer = Buyer.objects.get(user=request.user)

    except:
        raise Http404 

    designers = Designer.objects.all()
    if request.method == "POST":
        form = DesignRequestForm(request.POST, request.FILES)

        if form.is_valid():
            designer = Designer.objects.get(id=form.cleaned_data["designer"])

            design_request = DesignRequest.objects.create(
                buyer=buyer,
                designer=designer,
                title=form.cleaned_data["title"],
                description=form.cleaned_data["description"],
                price=form.cleaned_data["price"],
            )
            if form.cleaned_data["size"]:
                size_list = form.cleaned_data["size"]
                for size in size_list:
                    try:
                        size_obj = Size.objects.get(id=size)
                        design_request.size.add(size_obj)
                    except Size.DoesNotExist:
                        pass 
            
            if request.FILES:
                design_image = request.FILES.get("design_image")
                design_request.design_image = design_image

            design_request.save()

            msg = SendEmail(request, file=[design_request.design_image.path])
            msg.send(recipient=[design_request.designer.user.email], template_path='email_messages/new_design_request.html',
                context={
                    'design_request': design_request,
                },
                subject='Request For New Design!'
            )

            messages.add_message(request, messages.INFO, "Your Design request has been sent to successfully {}.".format(design_request.designer))

            return redirect(reverse('buyer_profile', kwargs={'username': design_request.buyer.user.username}))

        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}
                print tmp
    else:
        form = DesignRequestForm()

    return render_to_response("buyer/design_request.html", {
        "designers": designers,
        "form": form,
    }, context_instance=RequestContext(request))


@login_required
def requested_design(request):
    buyer = Buyer.objects.get(user=request.user)
    my_requests = DesignRequest.objects.filter(buyer=buyer)
    return render_to_response("buyer/my_request.html", {
        "my_requests": my_requests,                      
    }, context_instance=RequestContext(request)) 