from django.contrib import admin

from .models import Designer, Design, Size, DesignType, FinancialActivities


class DesignerAdmin(admin.ModelAdmin):
    list_display = ['user', 'phone_no', 'city', 'state', 'country']
    search_fields = ['user__first_name', 'user__last_name']


class DesignTypeAdmin(admin.ModelAdmin):
    list_display = ['types']
    search_fields = ['types']


class SizeAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']


class DesignAdmin(admin.ModelAdmin):
    list_display = ['designer', 'name', 'gender', 'no_of_views', 'price', 'stock', 'design_rating']
    filter_horizontal = ['size']
    search_fields = ['designer__user__first_name', 'designer__user__last_name', 'name']
    list_filter = ('designer',)


class FinancialActivitiesAdmin(admin.ModelAdmin):
    list_display = ['designer', 'design', 'price', 'status']
    search_fields = ['designer']


admin.site.register(Designer, DesignerAdmin)
admin.site.register(Design, DesignAdmin)
admin.site.register(Size, SizeAdmin)
admin.site.register(DesignType, DesignTypeAdmin)
admin.site.register(FinancialActivities, FinancialActivitiesAdmin)

