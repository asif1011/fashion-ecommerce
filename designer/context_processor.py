from django.conf import settings
from buyer.models import DesignRequest
from designer.models import Designer


def get_request_counter(request):
    if request.user.is_authenticated():
    	try:
    		designer = Designer.objects.get(user=request.user)
        	new_request = DesignRequest.objects.filter(designer = designer, request_status="Pending")
        	request_counter = len(new_request)
        	return {'request_counter': request_counter}
    	except:
    		return {}
    else:
        return {}