from __future__ import unicode_literals
from datetime import datetime

from django import forms
from django.conf import settings
from django.forms import FileInput
from django_countries import countries
from django.contrib.auth.models import User
from django.contrib.auth import password_validation
from designer.models import Designer, Design, Size, GENDER_CHOICES, CATEGORY_CHOICES


class SignUpDesignerForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    phone_no = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'maxlength': '15', 'minlength': '10'}))
    avatar = forms.ImageField(required=True)
    gender = forms.CharField(widget=forms.Select(choices=GENDER_CHOICES, attrs={'class': 'form-control'}))
    identity_document = forms.FileField(required=True)
    street1 = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'cols': '0', 'rows': '4'}), required=True)
    street2 =forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    city  = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=True)
    state = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=True)
    postal_code = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=True)
    country = forms.CharField(widget=forms.Select(choices=countries, attrs={'class': 'form-control'}), required=True)
    nationality = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=False)
    bio = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'cols': '0', 'rows': '0'}), required=False)        

    def clean_email(self):
        try:
            existing_user = User.objects.get(email__iexact=self.cleaned_data['email'])
            if existing_user:
                self._errors["email"] = self.error_class(["An account already exists under this email address. Please use the forgot password function to log in."])
        except User.MultipleObjectsReturned:
            self._errors["email"] = self.error_class(["An account already exists under this email address. Please use the forgot password function to log in."])
        except:
            pass

        return self.cleaned_data['email']

    def clean_phone_no(self):
        phone_no = self.cleaned_data.get('phone_no')

        try:
            phone_no = int(phone_no)
        except:
            raise forms.ValidationError('Enter a whole number.')

        return phone_no


class EditDesignerForm(forms.ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control', 'type': 'email', 'readonly': 'readonly'}))

    class Meta:
        model=Designer
        exclude = ['user', 'is_premium']
        widgets = {
            'avatar': FileInput(),
            'identity_document': FileInput(attrs={'style': 'padding-top: 15px'})
        }

    def __init__(self, *args, **kwargs):
        super(EditDesignerForm, self).__init__(*args, **kwargs)
        self.fields['phone_no'].widget.attrs.update({'class': 'form-control'})
        self.fields['street1'].widget.attrs.update({'class': 'form-control'})
        self.fields['street2'].widget.attrs.update({'class': 'form-control'})
        self.fields['city'].widget.attrs.update({'class': 'form-control'})
        self.fields['gender'].widget.attrs.update({'class': 'form-control'})
        self.fields['state'].widget.attrs.update({'class': 'form-control', 'cols': '', 'rows': ''})
        self.fields['postal_code'].widget.attrs.update({'class': 'form-control'})
        self.fields['country'].widget.attrs.update({'class': 'form-control'})
        self.fields['bio'].widget.attrs.update({'class': 'form-control', 'rows': '0', 'cols': '0', 'maxlength': '500'})
        self.fields['phone_no'].required = True
        self.fields['street1'].required = True
        self.fields['city'].required = True
        self.fields['state'].required = True


class UploadDesignForm(forms.Form):
    avatar = forms.ImageField(required=True)
    avatar_1 = forms.ImageField(required=True)
    avatar_2 = forms.ImageField(required=False)
    avatar_3 = forms.ImageField(required=False)
    avatar_4 = forms.ImageField(required=False)
    avatar_5 = forms.ImageField(required=False)
    design_type = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=True)
    gender = forms.CharField(widget=forms.Select(choices=CATEGORY_CHOICES, attrs={'class': 'form-control'}))
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'minlength': '4'}))
    size = forms.CharField(widget=forms.Select(attrs={'class': 'form-control multiselect mymultiselect', 'required': 'required'}))
    details = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'cols': '0', 'rows': '5'}), required=True)
    price = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Price'}))

    def __init__(self, *args, **kwargs):
        super(UploadDesignForm, self).__init__(*args, **kwargs)
        self.fields['size'] = forms.MultipleChoiceField(
            choices=[(size.id, str(size.name)) for size in Size.objects.all()]
        )
        self.fields['size'].required = True


    def clean_price(self):
        price = self.cleaned_data.get('price')

        try:
            price = float(price)
        except:
            raise forms.ValidationError('Enter a number.')

        return price


class EditDesignForm(forms.ModelForm):
    design_type = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), required=True)

    class Meta:
        model=Design
        exclude = ['designer', 'buyer', 'stock', 'no_of_views']
        widgets = {
            'avatar': FileInput(),
            'avatar_1': FileInput(),
            'avatar_2': FileInput(),
            'avatar_3': FileInput(),
            'avatar_4': FileInput(),
            'avatar_5': FileInput(),
        }

    def __init__(self, *args, **kwargs):
        super(EditDesignForm, self).__init__(*args, **kwargs)
        self.fields['avatar'].required = True
        self.fields['name'].widget.attrs.update({'class': 'form-control'})
        self.fields['gender'].widget.attrs.update({'class': 'form-control'})
        self.fields['price'].widget.attrs.update({'class': 'form-control'})
        self.fields['details'].widget.attrs.update({'class': 'form-control'})
        self.fields['size'] = forms.MultipleChoiceField(
            choices=[(size.id, str(size.name)) for size in Size.objects.all()]
        )
        self.fields['size'].required = True

    def clean_price(self):
        price = self.cleaned_data.get('price')

        try:
            price = float(price)
        except:
            raise forms.ValidationError('Enter a number.')

        return price