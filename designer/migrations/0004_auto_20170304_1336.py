# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-03-04 08:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('designer', '0003_auto_20170304_0123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='design',
            name='gender',
            field=models.CharField(choices=[('Men', 'Men'), ('women', 'Women'), ('Boys', 'Boys'), ('Girls', 'Girls')], max_length=60),
        ),
    ]
