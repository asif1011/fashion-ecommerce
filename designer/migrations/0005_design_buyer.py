# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-03-04 08:09
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('buyer', '0001_initial'),
        ('designer', '0004_auto_20170304_1336'),
    ]

    operations = [
        migrations.AddField(
            model_name='design',
            name='buyer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='buyer.Buyer'),
        ),
    ]
