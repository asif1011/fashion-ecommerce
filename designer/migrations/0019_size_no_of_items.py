# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-04-30 03:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('designer', '0018_design_design_rating'),
    ]

    operations = [
        migrations.AddField(
            model_name='size',
            name='no_of_items',
            field=models.CharField(blank=True, max_length=40, null=True),
        ),
    ]
