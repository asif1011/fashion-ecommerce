from __future__ import unicode_literals

import os
from datetime import datetime

from django.db import models
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django_countries.fields import CountryField
from django.core.files.storage import FileSystemStorage
from autoslug import AutoSlugField

from fashion.utils import SendEmail

media_storage = FileSystemStorage(location=settings.BASE_DIR, base_url="/media/")

GENDER_CHOICES = (
    ('Male', 'Male'),
    ('Female', 'Female'),
    ('Others', 'Others'),
)

CATEGORY_CHOICES = (
    ('Men', 'Men'),
    ('Women', 'Women'),
    ('Boys', 'Boys'),
    ('Girls', 'Girls')
)

PAYMENT_STATUS = (
    ('Pending', 'Pending'),
    ('Paid', 'Paid')
)

class Designer(models.Model):
    user = models.ForeignKey(User)
    avatar = models.ImageField(upload_to="profile/designer/", null=True, blank=True)
    phone_no = models.CharField(max_length=12, blank=True, null=True)
    gender = models.CharField(max_length=60, null=True, blank=True, choices=GENDER_CHOICES)
    street1 = models.CharField(max_length=60, blank=True, null=True)
    street2 = models.CharField(max_length=60, blank=True, null=True)
    city  = models.CharField(max_length=30, blank=True, null=True)
    state = models.CharField(max_length=30, blank=True, null=True)
    postal_code = models.CharField(max_length=12, blank=True, null=True)
    bio = models.TextField(null=True, blank=True)
    identity_document = models.FileField(upload_to="documents/identity/", null=True, blank=True)
    country = CountryField(blank_label="(select country)", null=True, blank=True)
    is_premium = models.BooleanField(default=False)

    def __str__(self):
        return u'%s %s' %(self.user.first_name, self.user.last_name)

    def get_identity_document(self):
        return os.path.basename(self.identity_document.name)


class Size(models.Model):
    slug = AutoSlugField(populate_from='name', unique=True)
    name = models.CharField(max_length=40)

    def __str__(self):
        return u'%s' %(self.name)


class DesignType(models.Model):
    slug = AutoSlugField(populate_from='types', unique=True)
    types = models.CharField(max_length=40)

    def __str__(self):
        return u'%s' %(self.types)


class Design(models.Model):
    avatar = models.ImageField(upload_to='design_images/', null=True, blank=True)
    avatar_1 = models.ImageField(upload_to='design_images/', null=True, blank=True)
    avatar_2 = models.ImageField(upload_to='design_images/', null=True, blank=True)
    avatar_3 = models.ImageField(upload_to='design_images/', null=True, blank=True)
    avatar_4 = models.ImageField(upload_to='design_images/', null=True, blank=True)
    avatar_5 = models.ImageField(upload_to='design_images/', null=True, blank=True)
    designer = models.ForeignKey(Designer)
    gender = models.CharField(max_length=60, choices=CATEGORY_CHOICES)
    design_type = models.ManyToManyField(DesignType)
    no_of_views = models.IntegerField(default=0, null=True, blank=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    size = models.ManyToManyField(Size)
    details = models.TextField(null=True, blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    stock = models.BooleanField(default=True)
    design_rating = models.FloatField(default=0.0, null=True, blank=True)


    def __str__(self):
        return u'%s' % (self.name)


class FinancialActivities(models.Model):
    designer = models.ForeignKey(Designer)
    design = models.ForeignKey(Design)
    transfer_id = models.CharField(max_length=255, null=True, blank=True)
    status = models.CharField(max_length=60, choices=PAYMENT_STATUS)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u'%s' % self.designer


def activate_account(sender, instance, **kwargs):
    if instance.id:
        old = instance.__class__.objects.get(id=instance.id)
        if not instance.is_active == old.is_active:
            user = User.objects.get(id=instance.id)
            password = user.password
            msg = SendEmail()
            msg.send(recipient=[user.email], template_path='email_messages/account_activated.html',
                context={
                    'user': user,
                },
                subject='Your Account has been activated successfully!'
            )

models.signals.pre_save.connect(activate_account, sender=User)
