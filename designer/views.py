import json, os
import random
from datetime import date, datetime, time, timedelta
from string import ascii_uppercase
from PIL import Image

from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib.auth import authenticate, login, get_backends
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.crypto import get_random_string
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q, Count
from django.http import HttpResponse, Http404
from django_countries import countries
from django.template.loader import render_to_string
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.core.files.base import ContentFile
from fashion.utils import SendEmail

from designer.utils import create_username
from designer.models import Designer, Design, Size, DesignType
from designer.forms import SignUpDesignerForm, EditDesignerForm, UploadDesignForm, EditDesignForm
from buyer.models import RecentlyViewed, Buyer, Feedback, DesignRequest
from buyer.forms import FeedbackForm


def homepage(request):
    if request.user.is_superuser:
        return render_to_response("admin.html", {  
        }, context_instance=RequestContext(request))
        
    all_designers = Designer.objects.all()[:3]
    all_designs = Design.objects.all().order_by('-no_of_views')[:6]

    return render_to_response("designer/homepage.html", {
        "all_designers": all_designers,
        "all_designs" :  all_designs
    }, context_instance=RequestContext(request))


def get_design(query=None, gender=None, design_type=None, size=None, designer=None, price=None):

    all_designs = Design.objects.all().order_by('-design_rating')

    if query:
        all_designs = all_designs.filter(name__icontains=query).order_by('-created')

    if gender:
        all_designs = all_designs.filter(gender__in=gender).distinct()
    
    if design_type:
        design_type = design_type.split(",")
        design_type = DesignType.objects.filter(types__in=design_type)
        all_designs = all_designs.filter(design_type__in=design_type).distinct()

    if size:
        size = Size.objects.filter(name__in=size)
        all_designs = all_designs.filter(size__in=size).distinct()

    if designer:
        designer = Designer.objects.filter(user__username__in=designer)
        print designer
        all_designs = all_designs.filter(designer__in=designer).distinct()


    if price:
        if price == "a":
            all_designs_a = all_designs.filter(price__lte=500.00)
            all_designs = all_designs_a

        if price == "b":
            all_designs_b = all_designs.filter(price__gte=500.00, price__lte=1000.00)
            all_designs = all_designs_b

        if price == "c":
            all_designs_c = all_designs.filter(price__gte=1000.00 , price__lte=1500.0)
            all_designs = all_designs_c

        if price == "d":
            all_designs_d = all_designs.filter(price__gte=1500.00 , price__lte=5000.0)
            all_designs = all_designs_d

        if price == "e":
            all_designs_e = all_designs.filter(price__gt=5000.00)
            all_designs = all_designs_e

    return all_designs        


def fashion_home(request):
    if request.user.is_superuser:
        return render_to_response("admin.html", {  
        }, context_instance=RequestContext(request))

    all_designers = Designer.objects.all()[:5]
    size_list = Size.objects.all().values_list("name", flat=True) 
    query = request.GET.get('q', '')
    gender = request.GET.getlist('gender', '')
    design_type = request.GET.get('design_type', '')
    size = request.GET.getlist('size', '')
    designer = request.GET.getlist('designer', '')
    price = request.GET.get('price', '')
    
    no_result = False

    all_designs = get_design(query, gender, design_type, size, designer, price)

    if not all_designs:
        all_designs = Design.objects.filter()
        no_result = True

    print designer

    return render_to_response("all_designs.html", {
        "query": query,
        "designer_list": designer, 
        "gender": gender,
        "no_result": no_result,
        "price": price,
        "size" : size,
        "size_list": size_list,
        "all_designers": all_designers,
        "all_designs":  all_designs
    }, context_instance=RequestContext(request))


def registration(request):
   
    if request.method == "POST":
        form = SignUpDesignerForm(request.POST, request.FILES)

        if form.is_valid():
            user = User.objects.create(
                username=create_username('%s %s' % (form.cleaned_data["first_name"], form.cleaned_data["last_name"])),
                first_name=form.cleaned_data["first_name"],
                last_name=form.cleaned_data["last_name"],
                email=form.cleaned_data["email"],
                is_active=False
            )

            designer = Designer.objects.create(
                user=user,
                phone_no=form.cleaned_data["phone_no"],
                street1=form.cleaned_data["street1"],
                street2=form.cleaned_data["street2"],
                city=form.cleaned_data["city"],
                state=form.cleaned_data["state"],
                postal_code=form.cleaned_data["postal_code"],
                country=form.cleaned_data["country"],
                bio=form.cleaned_data["bio"],
                gender=form.cleaned_data["gender"],
            )
            string = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()?"
            password = "".join(random.sample(string, 8))

            user.set_password(password)
            user.save()

            msg = SendEmail()
            msg.send(recipient=[designer.user.email], template_path='email_messages/new_designer_email_to_designer.html',
                context={
                    'designer': designer,
                    'password' : password
                },
                subject='Welcome to Fashion Evolution!'
            )

            if request.FILES:

                avatar = request.FILES.get("avatar")
                identity_document = request.FILES.get("identity_document")
                designer.avatar = avatar
                designer.identity_document = identity_document

            designer.save()

            admin_emails = User.objects.filter(is_superuser=True).values_list('email', flat=True)

            msg = SendEmail(request)
            msg.send(recipient=admin_emails, template_path='email_messages/new_designer_email_to_admin.html',
                context={
                    'designer_link': settings.SITE_URL+"/admin/designer/designer/{}".format(designer.id),
                    'designer': designer
                },
                subject='New Designer Registered!'
            )

            return redirect("/")

        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}
                print tmp    
            
    else:
        form = SignUpDesignerForm()

    return render_to_response("designer/registration.html", {
        "form": form,
    }, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def profile(request, username):
    
    designer = Designer.objects.get(user__username=username)

    if not request.user == designer.user:
        return(redirect("/"))

    else:    
        return render_to_response("designer/profile.html", {
            "current" : "profile",
            "designer" : designer,
            
        }, context_instance=RequestContext(request))


def get_dress_type(request):
    """
    Function is used to get the dress_ttypes.
    """
    response_data = []

    if request.is_ajax():
        types_obj = DesignType.objects.filter(types__icontains=request.GET.get('q'))

        for types in types_obj:
            response_data.append(types.types)

    else:
        data = "fail"

    mimetype = "application/json"
    return HttpResponse(json.dumps(response_data), mimetype)


@login_required(login_url=settings.LOGIN_URL)
def edit_designer(request):

    designer = Designer.objects.get(user=request.user)

    if request.method == "POST":
        form = EditDesignerForm(request.POST, request.FILES, instance=designer)

        if form.is_valid():
            obj = form.save(commit=False)
            obj.user.first_name = form.cleaned_data['first_name']
            obj.user.last_name = form.cleaned_data['last_name']

            obj.user.save()
            obj.save()

            messages.add_message(request, messages.INFO, "Hi {}, Your profile has been updated successfully.".format(designer.user.first_name))

            return redirect(reverse('profile', kwargs={'username': designer.user.username}))

        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}

        return render_to_response("designer/edit_profile.html", {
            "form": form,
            "designer": designer,
        }, context_instance=RequestContext(request))

    else:
        form = EditDesignerForm(instance=designer, initial={
            'first_name': designer.user.first_name,
            'last_name': designer.user.last_name,
            'email': designer.user.email,
        })

        return render_to_response("designer/edit_profile.html", {
            "current": "edit",
            "form": form,
            "designer": designer,
        }, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def upload_design(request):

    designer = Designer.objects.get(user=request.user)
    
    if not request.user == designer.user:
        return redirect("/")
    
    if request.method == "POST":
        form = UploadDesignForm(request.POST, request.FILES)

        if form.is_valid():
            design = Design.objects.create(
                designer=designer,
                gender=form.cleaned_data["gender"],
                name=form.cleaned_data["name"],
                details=form.cleaned_data["details"],
                price=form.cleaned_data["price"],
            )

            if form.cleaned_data["size"]:
                size_list = form.cleaned_data["size"]
                for size in size_list:
                    try:
                        size_obj = Size.objects.get(id=size)
                        design.size.add(size_obj)
                    except Size.DoesNotExist:
                        pass                

            if form.cleaned_data['design_type']:
                design_type_list = form.cleaned_data['design_type'].split(',')
                for types in design_type_list:
                    try:
                        types = DesignType.objects.get(types__iexact=types)
                    except DesignType.DoesNotExist:
                        types = types.strip()
                        if types:
                            types = DesignType.objects.create(types=types)

                    design.design_type.add(types)

            if request.FILES:

                avatar = request.FILES.get("avatar")
                design.avatar = avatar

                avatar_1 = request.FILES.get("avatar_1")
                design.avatar_1 = avatar_1

                avatar_2 = request.FILES.get("avatar_2")
                design.avatar_2 = avatar_2

                avatar_3 = request.FILES.get("avatar_3")
                design.avatar_3 = avatar_3

                avatar_4 = request.FILES.get("avatar_4")
                design.avatar_4 = avatar_4

                avatar_5 = request.FILES.get("avatar_5")
                design.avatar_5 = avatar_5
            design.save()
            messages.add_message(request, messages.INFO, "Hi {}, Your design has been uploaded successfully.".format(designer.user.first_name))

            return redirect(reverse('my_designs'))
            
        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}
                print tmp
            
    else:
        form = UploadDesignForm()

    return render_to_response("designer/upload_design.html", {
        "current": "upload_design",
        "form": form,
    }, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def my_designs(request):
    
    designer = Designer.objects.get(user=request.user)

    design_list = Design.objects.filter(designer=designer).order_by('-created')

    query = request.GET.get('q', '')
    if query:
        design_list = Design.objects.filter(name__icontains=query, designer=designer).order_by('-created')

    return render_to_response("designer/design_list.html", {
        "design_list":design_list,
        "current": "my_design",
    }, context_instance=RequestContext(request))


@login_required(login_url=settings.LOGIN_URL)
def design_details(request, design_id):
    
    design = Design.objects.get(id=design_id)

    all_feedbacks = Feedback.objects.filter(design=design).order_by('-created')

    return render_to_response("designer/design_detail.html", {
        "all_feedbacks":all_feedbacks,
        "design":design,
        "current": "design_detail",
    }, context_instance=RequestContext(request))  


@login_required(login_url=settings.LOGIN_URL)
def delete_design(request):
    response = {"status": False, "errors": []}
    design_id = request.GET.get('design_id', '')

    design = Design.objects.get(id=design_id)
    designer = Designer.objects.get(user=request.user)

    if design.designer == designer:
        design.delete()

    response["status"] = True
    return HttpResponse(json.dumps(response))


@login_required(login_url=settings.LOGIN_URL)
def edit_design(request, design_id):
    
    design = Design.objects.get(id=design_id)
    designer = Designer.objects.get(user=request.user)

    if not request.user == design.designer.user:
        messages.add_message(request, messages.ERROR, "Hi {}, You don't have access to edit this design.".format(designer.user.first_name))
        return redirect(reverse('my_designs'))

    if request.method == "POST":
        form = EditDesignForm(request.POST, request.FILES , instance=design)

        if form.is_valid():
            design.gender = form.cleaned_data["gender"]
            design.name = form.cleaned_data["name"]
            design.details = form.cleaned_data["details"]
            design.price = form.cleaned_data["price"]

            design.size.clear()
            if form.cleaned_data["size"]:
                size_list = form.cleaned_data["size"]
                for size in size_list:
                    try:
                        size_obj = Size.objects.get(id=size)
                        design.size.add(size_obj)
                    except Size.DoesNotExist:
                        pass

            design.design_type.clear()
            if form.cleaned_data['design_type']:
                design_type_list = form.cleaned_data['design_type'].split(',')
                for types in design_type_list:
                    try:
                        types = DesignType.objects.get(types__iexact=types)
                    except DesignType.DoesNotExist:
                        types = types.strip()
                        if types:
                            types = DesignType.objects.create(types=types)

                    design.design_type.add(types)

            if request.FILES:

                avatar = request.FILES.get("avatar")
                if avatar:
                    design.avatar = avatar

                avatar_1 = request.FILES.get("avatar_1")
                if avatar_1:
                    design.avatar_1 = avatar_1

                avatar_2 = request.FILES.get("avatar_2")
                if avatar_2:
                    design.avatar_2 = avatar_2

                avatar_3 = request.FILES.get("avatar_3")
                if avatar_3:
                    design.avatar_3 = avatar_3

                avatar_4 = request.FILES.get("avatar_4")
                if avatar_4:
                    design.avatar_4 = avatar_4

                avatar_5 = request.FILES.get("avatar_5")
                if avatar_5:
                    design.avatar_5 = avatar_5

            design.save()
            messages.add_message(request, messages.INFO, "Hi {}, Your design has been updated successfully.".format(designer.user.first_name))


            return redirect(reverse('my_designs'))
        else:
            for key, value in form.errors.items():
                tmp = {'key': key, 'error': value.as_text()}
                print tmp

    else:
        form = EditDesignForm(instance=design, initial={
            'name' : design.name,
            'price' : design.price,
            'details' : design.details,

        })            

    return render_to_response("designer/edit_design.html", {
        "current": "upload_design",
        "design" : design,
        "form": form,
    }, context_instance=RequestContext(request))


def product_details(request, product_name, product_id):
    design = Design.objects.get(id=product_id)

    all_ratings = Feedback.objects.filter(design=design).values_list("rating", flat=True)
    all_reviews = len(Feedback.objects.filter(design=design).values_list("reviews", flat=True))
    all_feedbacks = Feedback.objects.filter(design=design).order_by('-created')

    total_rating = int(sum(all_ratings))
    len_of_ratings = len(all_ratings)
    if len_of_ratings == 0:
        len_of_ratings = 1
    else:
        len_of_ratings = len(all_ratings)    
    average = float(total_rating)/float(len_of_ratings)
    rating = design.design_rating = format(average, '.1f')
    design.save()

    if not request.user == design.designer.user:
        design.no_of_views += 1
        design.save()

    if not request.user == design.designer.user and request.user.is_authenticated():
        recently_viewed, created = RecentlyViewed.objects.get_or_create(
            user = request.user,
            design = design
        )
        if not created:
            recently_viewed.save()
      
    return render_to_response("designer/product_detail.html", {
        "all_feedbacks":all_feedbacks,
        "all_reviews": all_reviews,
        "total_rating": total_rating,
        "rating":rating,
        "design":design,
        "form" :FeedbackForm,
        "current": "product_detail",
    }, context_instance=RequestContext(request))


def design_requests_designer(request):
    designer = Designer.objects.get(user=request.user)
    all_requests = DesignRequest.objects.filter(designer=designer).order_by('-created')

    return render_to_response("designer/design_request.html", {
        "all_requests":all_requests,
    }, context_instance=RequestContext(request))


def accept_reject_request(request):
    response = {}
    if request.is_ajax() and request.POST['action'] == 'accpetRequest':
        request_id = request.POST["request_id"]
        requested_design = DesignRequest.objects.get(id=request_id)
        requested_design.request_status = "Accepted"
        requested_design.save()

        msg = SendEmail()
        msg.send(recipient=[requested_design.buyer.user.email], template_path='email_messages/design_accepted.html',
            context={
                'request': requested_design,
            },
            subject='Congratulations! Your design request has been accepted.'
        )

        response = {"status": True}
        return HttpResponse(json.dumps(response))

    if request.is_ajax() and request.POST['action'] == 'rejectRequest':
        request_id = request.POST["request_id"]
        requested_design = DesignRequest.objects.get(id=request_id)
        reason = request.POST["reason"]
        if not reason == "":
            print "hererererer"
            requested_design.request_status = "Rejected"
            requested_design.reject_reason = reason
            requested_design.save()
            print requested_design.buyer.user.email

            msg = SendEmail()
            msg.send(recipient=[requested_design.buyer.user.email], template_path='email_messages/design_rejected.html',
                context={
                    'request': requested_design,
                },
                subject='Sorry!! Your design request has been Rejected'
            )
            
            response = {"status": True}
            return HttpResponse(json.dumps(response))

        else:
            print "threrere"
            response = {"status": False, "text": "Please Provide the reason"}
            return HttpResponse(json.dumps(response))




