from designer.models import Designer
from buyer.models import Buyer, CartItems
from django.contrib.auth.models import User


def user_info(request):
    if request.user.is_authenticated():
        try:
            buyer = Buyer.objects.get(user=request.user)
            context = {
                'role': 'buyer',
                'profile': buyer
            }
        except:
            try:
                designer = Designer.objects.get(user=request.user)
                context = {
                    'role': 'designer',
                    'profile': designer
                }
            except:
                context = {
                    'role': 'admin'
                }
    else:
        context = {}

    return context


def get_cart_counter(request):
    get_user = user_info(request)
    if request.user.is_authenticated():
        if not get_user["role"] == "designer" and not request.user.is_superuser:  
            buyer = Buyer.objects.get(user=request.user)
            my_carts = CartItems.objects.filter(buyer=buyer)
            cart_items_len = my_carts.count()
            return {'cart_items_len': cart_items_len}
        else:
            return {}
    else:
        return {}        
