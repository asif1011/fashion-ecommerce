from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.auth import views
from django.views.static import serve
from django.conf import settings


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

    # Home Url
    url(r'^$', 'designer.views.homepage', name='homepage'),
    url(r'^about-us/$', 'accounts.views.about_us', name='about_us'),
    url(r'^contact-us/$', 'accounts.views.contact_us', name='contact_us'),
    url(r'^newsletter/$', 'accounts.views.newsletter', name='newsletter'),
    url(r'^blog/$', 'accounts.views.blog', name='blog'),

    url(r'^design/design-detail/(?P<product_name>[\w-]+)/(?P<product_id>[0-9]+)/$', 'designer.views.product_details', name='product_details'),

    url(r'^all-designs/$', 'designer.views.fashion_home', name='fashion_home'),

    # App Url
    url(r'buyer/', include('buyer.urls')),
    url(r'designer/', include('designer.urls')),
    url(r'accounts/', include('accounts.urls')),

    # Logout Url
    url(r'^logout/$', views.logout, {'next_page': settings.LOGIN_REDIRECT_URL}, name="logout"),

    # Reset Password Url
    url(r'^user/password/reset/done/$',
        'django.contrib.auth.views.password_reset_done',
         name='password_reset_done'),

    # Media Url
    url(r'^media/(?P<path>.*)', serve, {"document_root": settings.MEDIA_ROOT})
]
